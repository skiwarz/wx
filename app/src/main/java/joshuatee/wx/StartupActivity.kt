package joshuatee.wx

import android.os.Bundle
import android.app.Activity
import joshuatee.wx.notifications.UtilityWXJobService
import joshuatee.wx.objects.Route
import joshuatee.wx.settings.Location
import joshuatee.wx.settings.UtilityStorePreferences
import joshuatee.wx.util.Utility

class StartupActivity : Activity() {

    //
    // This activity is the first activity started when the app starts.
    // It's job is to initialize preferences if not done previously,
    // display the splash screen, start the service that handles notifications,
    // and display the version in the title.
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPreferences()
        initBackgroundJobs()
        startInitialActivity()
        finish()
    }

    private fun initPreferences() {
        if (Utility.readPrefWithNull(this, "LOC1_LABEL", null) == null) {
            UtilityStorePreferences.setDefaults(this)
        }
        MyApplication.initPreferences(this)
        Location.refreshLocationData(this)
    }

    private fun initBackgroundJobs() {
        UtilityWXJobService.startService(this)
    }

    private fun startInitialActivity() {
        Route(this, WX::class.java)
    }
}
